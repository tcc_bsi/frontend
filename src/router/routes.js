const routes = [{
    path: '/',
    component: () => import('layouts/MyLayout.vue'),
    children: [{
        path: '',
        component: () => import('pages/Index.vue')
      },
      {
        path: 'index',
        component: () => import('pages/Index.vue')
      },
      {
        path: 'perfil',
        component: () => import('pages/Profile.vue')
      },
      {
        path: 'alunos',
        component: () => import('pages/Student.vue')
      },
      {
        path: 'modalidades',
        component: () => import('pages/Modality.vue')
      },
      {
        path: 'cursos_turmas',
        component: () => import('pages/ClassCourse.vue')
      },
      {
        path: 'eventos',
        component: () => import('pages/Event.vue')
      },
      {
        path: 'atividades',
        name: 'atividadesName',
        component: () => import('pages/Activity.vue'),
      },
      {
        path: 'atletas',
        name: 'atletasName',
        component: () => import('pages/Athlete.vue'),
      },
      {
        path: 'usuarios',
        component: () => import('pages/users.vue'),
      }
    ]
  },
  {
    path: '/login',
    component: () => import('layouts/LoginLayout.vue'),
    children: [{
      path: '',
      component: () => import('pages/Login.vue'),
    }]
  },
  {
    path: '/cadastrar',
    component: () => import('layouts/RegisterLayout.vue'),
    children: [{
        path: 'administrador',
        component: () => import('pages/RegisterAdmin.vue'),
      },
      {
        path: 'professor',
        component: () => import('pages/RegisterManager.vue'),
      },
      {
        path: 'bolsista',
        component: () => import('pages/RegisterHolder.vue'),
      }
    ]

  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
