import Vue from 'vue'
import Vuelidate from 'vuelidate'

// "async" is optional
export default async ({
  Vue
}) => {
  Vue.use(Vuelidate)
}
