// import something here
import Gate from '../../.quasar/Gate'
//import Dragula-vue2
import {
  Vue2Dragula
} from 'vue2-dragula'
// "async" is optional
export default async ({
  /* app, router, Vue, ... */
  Vue
}) => {
  Vue.prototype.$gate = new Gate(localStorage.type);
  console.log('LOCALSTORAGE:: ' + localStorage.type);
  Vue.use(Vue2Dragula, {
    logging: {
      service: true // to only log methods in service (DragulaService)
    }
  });
}
